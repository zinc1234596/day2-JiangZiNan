package pos.machine;

import java.util.*;
import java.util.stream.Collectors;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        if (isValid(barcodes)) {
            List<Goods> goodsList = buildGoodsList(barcodes);
            return buildReceipt(goodsList);
        } else {
            return "";
        }

    }

    public boolean isValid(List<String> barcodes) {
        return !barcodes.isEmpty();
    }

    public List<Goods> buildGoodsList(List<String> barcodes) {
        List<Item> items = ItemsLoader.loadAllItems();
        return barcodes.stream().distinct().map(barcode -> {
            int count = Collections.frequency(barcodes, barcode);
            return items.stream().filter(item -> item.getBarcode().equals(barcode)).findFirst().map(item -> new Goods(item, count)).orElse(null);
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }


    public String buildReceipt(List<Goods> goodsList) {
        List<String> allGoodsPrintInfoList = buildAllGoodsLines(goodsList);
        int totalPrice = calculateTotalPrice(goodsList);
        return combineReceipt(allGoodsPrintInfoList, totalPrice);
    }

    public String buildSingleGoodLine(Goods goods) {
        int count = goods.getCount();
        Item item = goods.getItem();
        int sum = count * item.getPrice();
        int price = item.getPrice();
        return "Name: " + item.getName() + ", Quantity: " + count + ", Unit price: " + price + " (yuan), Subtotal: " + sum + " (yuan)\n";
    }

    public List<String> buildAllGoodsLines(List<Goods> goodsList) {
        return goodsList.stream().map(this::buildSingleGoodLine).collect(Collectors.toList());
    }

    public int calculateTotalPrice(List<Goods> goodsList) {
        return goodsList.stream().mapToInt(goods -> goods.getCount() * goods.getItem().getPrice()).sum();
    }

    public String combineReceipt(List<String> allGoodsPrintInfoList, int totalPrice) {
        StringBuilder receiptBuilder = new StringBuilder();
        receiptBuilder.append("***<store earning no money>Receipt***\n");

        for (String goodsPrintInfo : allGoodsPrintInfoList) {
            receiptBuilder.append(goodsPrintInfo);
        }

        receiptBuilder.append("----------------------\n");
        receiptBuilder.append("Total: ").append(totalPrice).append(" (yuan)\n");
        receiptBuilder.append("**********************");

        return receiptBuilder.toString();
    }
}
