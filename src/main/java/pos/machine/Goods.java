package pos.machine;

public class Goods {
    private final Item item;
    private final int count;

    public Goods(Item item, int count) {
        this.item = item;
        this.count = count;
    }

    public Item getItem() {
        return item;
    }

    public int getCount() {
        return count;
    }
}
